from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from fake_useragent import UserAgent
from selenium.webdriver.chrome.options import Options
import time
import pandas as pd

options = Options()
ua = UserAgent()
user_agent = ua.random
options.add_argument(f'user-agent={user_agent}')

service = Service(executable_path=ChromeDriverManager().install())
driver = webdriver.Chrome(service=service, options=options)
driver.maximize_window()

driver.get('https://www.watsons.com.hk/')

time.sleep(3)

# go to AS Watsons
navigation = driver.find_elements(By.CSS_SELECTOR, '.NavigationBar .nav-item')

category_url_list = []

# scrape the category list
for category in navigation:
    # print(123)
    # print(category.get_attribute('href'))
    category_url_list.append(category.get_attribute('href'))

# scrape the sub category list
all_sub_category_url_list = []
products_data = []

stop_count = 0
for category_url in category_url_list:
    print(f'#---------------------------- Category URL : {category_url}-----------------------------')
    driver.get(category_url)

    # wait for website to load
    time.sleep(3)

    sub_navigation_boxes = driver.find_elements(By.CSS_SELECTOR, '.box-menu .box')

    if sub_navigation_boxes is not None:
        for sub_navigation_box in sub_navigation_boxes:
            sub_navigation_box_urls = sub_navigation_box.find_elements(By.CSS_SELECTOR, '.box-content a')

            for sub_navigation_box_url in sub_navigation_box_urls:
                all_sub_category_url_list.append(sub_navigation_box_url.get_attribute('href'))

    print(f'#---------------------------- End Category URL : {category_url}-----------------------------')
    print(len(all_sub_category_url_list))
    print(all_sub_category_url_list)

    if len(all_sub_category_url_list) != 0:

        for sub_category_url in all_sub_category_url_list:
            print(f'#---------------------------Sub Category URL-{sub_category_url}-----------------------------')

            page = 0

            print(f'first : stop_count : {stop_count}')
            if stop_count == 1:
                print('break all_sub_category_url_list loop')
                break

            while True:
                driver.get(f'{sub_category_url}?currentPage={page}')
                # wait for website to load
                time.sleep(3)

                product_list = driver.find_elements(By.CSS_SELECTOR, '.productContainer')

                print('################## Page number : '+ str(page))
                print('################## No. of product : '+str(len(product_list)))

                if len(product_list) == 0:
                    print('No Product Found')

                    # for testing use only
                    stop_count += 1
                    break

                for product in product_list:
                    url = product.find_element(By.CSS_SELECTOR, '.productImage > a').get_attribute('href')
                    image_url = product.find_element(By.CSS_SELECTOR, '.productImage img').get_attribute('src')
                    name = product.find_element(By.CSS_SELECTOR, '.productName a').text

                    print('Product Name : ' + name)

                    if len(product.find_elements(By.CSS_SELECTOR, '.productPrice del')) > 0:
                        original_price = product.find_element(By.CSS_SELECTOR, '.productPrice del').text

                    if len(product.find_elements(By.CSS_SELECTOR, '.productPrice')) > 0:
                        price = product.find_element(By.CSS_SELECTOR, '.productPrice').text

                    products_data.append([sub_category_url, url, image_url, name, original_price, price])

                # next page number
                page += 1

            print(f'#----------------------------End Get Sub Category Products-----------------------------')

    print(f'second : stop_count : {stop_count}')
    if stop_count == 1:
        print('break category_url_list loop')
        break

data = pd.DataFrame(products_data, columns=['Sub Category URL', 'Product URL', 'Image URL ', 'Name', 'Original Price', 'Price'])

print(data)

data.to_csv('data/products_data.csv')

driver.close()